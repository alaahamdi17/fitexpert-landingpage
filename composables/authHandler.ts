import { ref, reactive, computed } from "vue";

import { useVuelidate } from "@vuelidate/core";
import { required, minLength, maxLength, email } from "@vuelidate/validators";

interface State {
  name: string | null;
  email: string | null;
  password: string | null;
}

const activeModal = ref<null | string>(null);
const AuthForm = ref(null);
const loading = ref(false);

export default function () {
  const router = useRouter();
  const route = useRoute();

  const state = reactive<State>({
    name: null,
    email: null,
    password: null,
  });

  const rules = computed(() => {
    if (activeModal.value === "login") {
      return {
        email: { required, email },
        password: {
          required,
          minLength: minLength(8),
          maxLength: maxLength(30),
        },
      };
    } else {
      return {
        name: { required, minLength: minLength(2), maxLength: maxLength(30) },
        email: { required, email },
        password: {
          required,
          minLength: minLength(8),
          maxLength: maxLength(30),
        },
      };
    }
  });
  // @ts-ignore
  const v$ = useVuelidate(rules, state);
  const openModal = (type: "register" | "login") => {
    router.currentRoute.value.query = { _auth_modal: null };
    resetForm();
    if (type === "login") {
      router.push({
        name: route.name || "index",
        query: { _auth_modal: type },
      });
    } else {
      router.push({
        name: route.name || "index",
        query: { _auth_modal: type },
      });
    }
  };
  const closeModal = () => {
    activeModal.value = null;
    router.push(route.path);
  };

  const submit = async (e: Event, type: "login" | "register") => {
    e.preventDefault();
    await v$.value.$validate();

    const payload = computed(() => {
      if (activeModal.value === "login") {
        return {
          email: state.email,
          password: state.password,
        };
      } else {
        return {
          name: state.name,
          email: state.email,
          password: state.password,
        };
      }
    });
    if (!v$.value.$invalid) {
      loading.value = true;
      setTimeout(() => {
        loading.value = false;
        console.log(type, payload.value);
      }, 3000);
    }
  };

  const resetForm = () => {
    if (AuthForm.value == null) return;
    v$.value.$reset();
    // @ts-expect-error
    AuthForm.value.reset();
  };
  return {
    openModal,
    activeModal,
    closeModal,
    submit,
    v$,
    state,
    AuthForm,
    loading,
  };
}
