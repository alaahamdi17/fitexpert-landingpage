// import BlogImg from "~/assets/imgs/blogs/blog_img.png";
// import { SingleBlog } from "~~/assets/types/BlogsType";
// interface State {
//   data: SingleBlog[] | null;
//   loading: Boolean;
//   singleBlog: {
//     data: SingleBlog | null;
//     loading: Boolean;
//     error: null | String;
//   };
// }
// export default function () {
//   const state = reactive<State>({
//     data: [
//       {
//         id: 12,
//         category: "Client Management",
//         title: "Managing Office Culture",
//         img: BlogImg,
//         info: "Lorem Ipsum is simply dummy text of the printingmplyprintingmplyprintingmplyprintingmplyprintingmplyprintingmply dummy text of the printingmply dummy text of the printingmply dummy text of the printingmply dummy text of the printingmply dummy text of the printingmply dummy text of the printing.",
//         descs: [
//           {
//             title: "Lorem Ipsum is simply dummy",
//             info: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into Aldus PageMaker including versions of Lorem Ipsum.",
//           },
//           {
//             title: "Lorem Ipsum is simply dummy",
//             info: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into Aldus PageMaker including versions of Lorem Ipsum.",
//           },
//         ],
//         relatedBlogs: {
//           next: {
//             title: "Managing office culture",
//             id: 12,
//           },
//           prev: {
//             title: "Managing office culture",
//             id: 12,
//           },
//         },
//       },
//       {
//         id: 12,
//         category: "Client Management",
//         title: "Managing Office Culture",
//         img: BlogImg,
//         info: "Lorem Ipsum is simply dummy text of the printing.",
//         descs: [
//           {
//             title: "Lorem Ipsum is simply dummy",
//             info: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into Aldus PageMaker including versions of Lorem Ipsum.",
//           },
//           {
//             title: "Lorem Ipsum is simply dummy",
//             info: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into Aldus PageMaker including versions of Lorem Ipsum.",
//           },
//         ],
//         relatedBlogs: {
//           next: {
//             title: "Managing office culture",
//             id: 12,
//           },
//           prev: {
//             title: "Managing office culture",
//             id: 12,
//           },
//         },
//       },
//       {
//         id: 12,
//         category: "Client Management",
//         title: "Managing Office Culture",
//         img: BlogImg,
//         info: "Lorem Ipsum is simply dummy text of the printing.",
//         descs: [
//           {
//             title: "Lorem Ipsum is simply dummy",
//             info: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into Aldus PageMaker including versions of Lorem Ipsum.",
//           },
//           {
//             title: "Lorem Ipsum is simply dummy",
//             info: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into Aldus PageMaker including versions of Lorem Ipsum.",
//           },
//         ],
//         relatedBlogs: {
//           next: {
//             title: "Managing office culture",
//             id: 12,
//           },
//           prev: {
//             title: "Managing office culture",
//             id: 12,
//           },
//         },
//       },
//       {
//         id: 12,
//         category: "Client Management",
//         title: "Managing Office Culture",
//         img: BlogImg,
//         info: "Lorem Ipsum is simply dummy text of the printing.",
//         descs: [
//           {
//             title: "Lorem Ipsum is simply dummy",
//             info: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into Aldus PageMaker including versions of Lorem Ipsum.",
//           },
//           {
//             title: "Lorem Ipsum is simply dummy",
//             info: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into Aldus PageMaker including versions of Lorem Ipsum.",
//           },
//         ],
//         relatedBlogs: {
//           next: {
//             title: "Managing office culture",
//             id: 12,
//           },
//           prev: {
//             title: "Managing office culture",
//             id: 12,
//           },
//         },
//       },
//     ],
//     singleBlog: {
//       data: null,
//       loading: true,
//       error: null,
//     },
//     loading: false,
//   });
//   const pagination = reactive({
//     perPage: 10,
//     total: 200,
//     page: 1,
//   });

//   const getSingleBlog = (id: number) => {
//     if (state.data?.length === 0 || !id) return;
//     // @ts-ignore
//     const blog = state.data.find((x) => x.id === parseInt(id));
//     console.log(blog);
//     if (typeof blog == "undefined") {
//       //-- fire request
//       console.log(blog);
//       return;
//     }
//     // @ts-ignore
//     state.singleBlog.data = blog;
//     state.singleBlog.loading = false;
//     state.singleBlog.error = null;
//     //-- fetch request from server
//   };
//   return { pagination, state, getSingleBlog };
// }

// @ts-ignore
import api from "~/composables/helpers/useRequestHandler";
// @ts-ignore
import BlogImg from "~/assets/imgs/blogs/blog_img.png";
import { SingleBlog } from "~~/assets/types/BlogsType";
interface State {
  data: SingleBlog[] | null;
  loading: Boolean;
  SingleBlog: {
    data: SingleBlog | null;
    loading: Boolean;
    error: null | String;
  };
}

const state = reactive<State>({
  data: [],
  SingleBlog: {
    data: null,
    loading: true,
    error: null,
  },
  loading: false,
});
const pagination = reactive({
  perPage: null,
  total: null,
});
const current_page = ref(1);
export default function () {
  const getSingleBlog = async (id: number) => {
    // if (state.data?.length === 0 || !id) return;
    // @ts-ignore
    state.SingleBlog.data = null;
    state.SingleBlog.loading = true;
    try {
      const { data } = await api().get(`/web/screens/blogs/${id}`);
      // console.log(data.data);
      state.SingleBlog.data = data.data;
      state.SingleBlog.loading = false;
      state.SingleBlog.error = null;
    } catch (err) {
      console.log(err);
    }
    // if (typeof Case == "undefined") {
    //   //-- fire request
    //   console.log(Case);
    //   return;
    // }
    // // @ts-ignore

    //-- fetch request from server
  };
  const fetchData = async () => {
    state.loading = true;
    try {
      const { data } = await api().get(
        `/web/screens/blogs?page=${current_page.value}`
      );
      console.log(data.data);
      const res = data.data;
      state.data = res.items;
      current_page.value = res.paginate.current_page;
      pagination.total = res.paginate.total;
      pagination.perPage = res.paginate.per_page;
      state.loading = false;
      window.scrollTo({
        left: 0,
        top: 0,
      });
    } catch (err) {
      state.loading = false;
      console.log(err);
    }
  };
  return { pagination, state, getSingleBlog, fetchData, current_page };
}
