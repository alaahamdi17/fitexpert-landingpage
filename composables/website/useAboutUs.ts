import api from "~/composables/helpers/useRequestHandler";

interface AboutUsData {
  basic_info: {};
  partners: string[];
  sections_info: {};
  missions: string[];
  metrics: string[];
  client_testimonials: {};
  setting:{};
}



const stateAboutUs = reactive<AboutUsData>({
  basic_info: '',
  partners: [],
  sections_info: '',
  missions: [],
  metrics: [],
  client_testimonials: '',
  setting: '',
});

export default function () {
  const fetchData = async () => {
    const { data } = await api().get("/web/screens/about-us");
    console.log("data", data.data);
    const res = data.data;

    stateAboutUs.basic_info = res.basic_info;
    stateAboutUs.partners = res.partners;
    stateAboutUs.sections_info = res.sections_info;
    stateAboutUs.missions = res.missions;
    stateAboutUs.metrics = res.metrics;
    stateAboutUs.client_testimonials = res.client_testimonials;
    stateAboutUs.setting = res.setting;
  };

  return { stateAboutUs, fetchData };
}
