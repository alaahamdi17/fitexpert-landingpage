import api from "~/composables/helpers/useRequestHandler";
const state = reactive({
  title: "",
  body: "",
});
export default function () {
  const fetchData = async () => {
    const { data } = await api().get("/web/screens/privacy-and-policy");
    console.log("data", data.data);
    const res = data.data;

    state.title = res.title;
    state.body = res.body;
  };

  return { state, fetchData };
}
