// @ts-ignore
import api from "~/composables/helpers/useRequestHandler";
// @ts-ignore
import BlogImg from "~/assets/imgs/blogs/blog_img.png";
import { SingleCase } from "~~/assets/types/CaseType";
interface State {
  data: SingleCase[] | null;
  loading: Boolean;
  singleCase: {
    data: SingleCase | null;
    loading: Boolean;
    error: null | String;
  };
}
const state = reactive<State>({
  data: [],
  singleCase: {
    data: null,
    loading: true,
    error: null,
  },
  loading: false,
});
const pagination = reactive({
  perPage: null,
  total: null,
});
const current_page = ref(1);
export default function () {
  const getSinglecase = async (id: number) => {
    // if (state.data?.length === 0 || !id) return;
    // @ts-ignore
    state.singleCase.data = null;
    state.singleCase.loading = true;
    try {
      const { data } = await api().get(`/web/screens/case-studies/${id}`);
      // console.log(data.data);
      state.singleCase.data = data.data;
      state.singleCase.loading = false;
      state.singleCase.error = null;
    } catch (err) {
      console.log(err);
    }
    // if (typeof Case == "undefined") {
    //   //-- fire request
    //   console.log(Case);
    //   return;
    // }
    // // @ts-ignore

    //-- fetch request from server
  };
  const fetchData = async () => {
    state.loading = true;
    try {
      const { data } = await api().get(
        `/web/screens/case-studies?page=${current_page.value}`
      );
      console.log(data.data);

      const res = data.data;
      state.data = res.items;
      current_page.value = res.paginate.current_page;
      pagination.total = res.paginate.total;
      pagination.perPage = res.paginate.per_page;
      state.loading = false;
      window.scrollTo({
        left: 0,
        top: 0,
      });
    } catch (err) {
      state.loading = false;
      console.log(err);
    }
  };
  return { pagination, state, getSinglecase, fetchData, current_page };
}
