import api from "~/composables/helpers/useRequestHandler";
const state = reactive({
  setting: {},
  sponsers: [],
  metrics: [],
  features: [],
  client_testimonials: {},
  case_studies: [],
  faqs: [],
  loaded: false,
});
export default function () {
  const fetchData = async () => {
    if (state.loaded) {
      return;
    }

    const { data } = await api().get("/web/screens/home");
    console.log("data", data.data);
    const res = data.data;

    state.setting = res.setting;
    state.sponsers = res.partners;
    state.metrics = res.metrics;
    state.features = res.features;
    state.client_testimonials = res.client_testimonials;
    state.case_studies = res.case_studies;
    state.faqs = res.faqs;
  };

  return { state, fetchData };
}
