import api from "~/composables/helpers/useRequestHandler";
const state = reactive({
  title: "",
  subtitle: "",
  features: {},
});

export default function () {
  const fetchData = async () => {
    const { data } = await api().get("/web/screens/features");
    console.log("data", data.data);
    const res = data.data;

    state.title = res.title;
    state.subtitle = res.subtitle;
    state.features = res.features;
  };

  return { state, fetchData };
}
