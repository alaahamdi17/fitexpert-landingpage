import { useVuelidate } from "@vuelidate/core";
import { required, email } from "@vuelidate/validators";
import api from "~/composables/helpers/useRequestHandler";

interface State {
  name: string;
  email: string;
  msg: string;
}

interface HtmlState {
  book_demo_text: string;
  doc_text: string;
  form_text: string;
  faqs: string[];
  loaded: false;
}

const stateHtml = reactive<HtmlState>({
  book_demo_text: "",
  doc_text: "",
  form_text: "",
  faqs: [],
  loaded: false,
});

export default function () {
  const fetchData = async () => {
    if (stateHtml.loaded) {
      return;
    }

    const { data } = await api().get("/web/screens/contact-us");
    console.log("data", data.data);
    const res = data.data;

    stateHtml.book_demo_text = res.book_demo_text;
    stateHtml.doc_text = res.doc_text;
    stateHtml.form_text = res.form_text;
    stateHtml.faqs = res.faqs;
  };

  const stateForm = reactive<State>({
    name: "",
    email: "",
    msg: "",
  });

  const rules = {
    name: { required },
    email: { required, email },
    msg: { required },
  };

  // @ts-ignore

  const v$ = useVuelidate(rules, stateForm);
  const submit = async () => {
    v$.value.$validate();

    if (v$.value.$invalid) {
      console.log("validate", v$.value);
      return;
    }
    try {
      const { data } = await api().post("/web/actions/contact-us", {
        message: stateForm.msg,
        email: stateForm.email,
        name: stateForm.name,
      });
      console.log("data", data);
    } catch (e) {
      console.log(e);
    }
  };

  return { stateForm, v$, fetchData, rules, stateHtml, submit };
}
