// https://nuxt.com/docs/api/configuration/nuxt-config
const meta = {
  titleTemplate: "%s - Fitexpert",
  title: "Fitexpert",
  meta: [
    {
      charset: "utf-8",
    },
    {
      name: "viewport",
      content: "width=device-width, initial-scale=1",
    },
    {
      hid: "description",
      name: "description",
      content: "",
    },
    {
      name: "format-detection",
      content: "telephone=no",
    },
  ],
  link: [
    {
      rel: "icon",
      type: "image/x-icon",
      href: "/logo.svg",
    },
  ],
};
export default defineNuxtConfig({
  typescript: {
    shim: false,
  },
  ssr: false,
  target: 'static',
  // @ts-ignore
  runtimeConfig: {
    public: {
      baseURL: process.env.BASE_URL || "no url",
    },
  },
  css: ["vuetify/styles", "@/assets/css/index.css"],
  modules: [
    "@pinia/nuxt",
    "@nuxtjs/tailwindcss",
    "nuxt-swiper",
    "@nuxt/image-edge",
  ],
  buildModules: ["@nuxt/postcss8", "@nuxt/image-edge"],
  build: {
    transpile: ["gsap"],
  },
  //-- meta tags
  app: {
    head: meta,
  },
  routeRules: {
    "/": {
      ssr: true,
    },
  },
});
