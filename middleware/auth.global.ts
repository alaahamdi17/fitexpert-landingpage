import useAuthHandler from "~~/composables/authHandler";
export default defineNuxtRouteMiddleware((to, from) => {
  const { activeModal } = useAuthHandler();
  // @ts-check
  to.query._auth_modal
    ? (activeModal.value = to.query._auth_modal)
    : (activeModal.value = null);
});
