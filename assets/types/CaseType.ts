interface SingleCase {
  id: number;
  title: string;
  image: string;
  body: string;
  description: string;
}

export { SingleCase };
