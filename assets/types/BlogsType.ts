interface SingleBlog {
  id: number;
  title: string;
  image: string;
  body: string;
  description: string;
  relatedBlogs: RelatedBlogs;
}



export interface RelatedBlogs {
  next_blog: Next;
  previous_blog: prev;
}

export interface Next {
  title: string;
  id: number;
  body: string;
  description: string;
  image: string;
}
export interface prev {
  title: string;
  id: number;
  body: string;
  description: string;
  image: string;
}
export { SingleBlog };
