interface SingleFeature {
  slug: string;
  title: string;
  subtitle: string;
  heroImg: string;
  section: SingleSection[];
}
interface SingleSection {
  title: string;
  desc: string;
  heroImg: string;
  benefits: SingleBenefit[];
}
interface SingleBenefit {
  title: string;
  desc: string;
}
export { SingleFeature };
