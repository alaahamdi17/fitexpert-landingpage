import { defineNuxtPlugin } from "#imports";
import NaiveUi from "naive-ui";

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.use(NaiveUi);
});
